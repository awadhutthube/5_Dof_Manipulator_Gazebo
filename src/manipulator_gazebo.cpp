/*
Gazebo Plugin for manipulator control. Dependencies - gazebo_ros , roscpp
Joint control is achieved through base functions from /usr/include/gazebo-x/gazebo/physics
File is referenced as library in CMAKELISTS.txt
*/

/////////////////////////////////////////////////////////////////////////////////////////////
#include <algorithm>
#include <boost/bind.hpp>
#include <chrono>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>  // joint.hh joint_controller.hh contain the functions for joint control
#include <gazebo/common/common.hh>
#include <iostream>
#include <thread>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>

//////////////////////////// variables declarations and initialization //////////////////////////////


int i = 1;
float arrays[5][8] = {{0,90, 90, 90, 90, 90, 90, 90},                        // array of angles to write
                      {0,26.17,32.675, 48.03, 65.96, 81.04, 91.09, 94.74},
                      {0,-120.91, -123.75, -129.06, -131.91, -129.06, -123.75, -120.91},
                      {0,94.74, 91.09, 81.04, 65.96,48.03, 32.67, 26.17},
                      {0,0, 0, 0, 0, 0, 0, 0}};

float vel_0,vel_1,vel_2,vel_3,vel_4,err_0,err_1,err_2,err_3,err_4;

float goal_0 = 90.0,goal_1 = 26.0,goal_2 = -120,goal_3 = 94.0,goal_4 = 0.0;    // initial pose before starting straight line


//////////////////////////////////// classes and callbacks /////////////////////////////////////////
namespace gazebo {

    class BasePlugin : public ModelPlugin {


        // Pointer to the model
        private: std::unique_ptr<ros::NodeHandle> rosNode;

        private: ros::Subscriber rosSub;

        private: ros::Subscriber rosSub1;

        private: ros::CallbackQueue rosQueue;

        private: std::thread rosQueueThread;

        private: physics::WorldPtr world;

        private: physics::ModelPtr model;

        private: physics::JointPtr joint_0;                                    // creating joint pointers for access to joints

        private: physics::JointPtr joint_1;

        private: physics::JointPtr joint_2;

        private: physics::JointPtr joint_3;

        private: physics::JointPtr joint_4;
        // Pointer to the update event connection
        private: event::ConnectionPtr updateConnection;

        public: float x = 0.0, y = 0.0, z = 0.0, s = 0.0;

        /**
         * Movement for desired pose in world frame.
         *
         * To be executed by the communications module.
         *
         * @param x
         * @param y
         * @param theta
         */

        private: void QueueThread()
        {
            static const double timeout = 0.01;
            while (this->rosNode->ok())
            {
                this->rosQueue.callAvailable(ros::WallDuration(timeout));
            }
        }

        public: void avitra_callback(const geometry_msgs::Twist::ConstPtr &cmd_msg)        // callback for base motion not used for manipulator
        {
            ROS_INFO("in callback");
            x = cmd_msg->linear.x;
            y = cmd_msg->linear.y;
            z = cmd_msg->angular.z;

        }

        public: void arm_callback(const geometry_msgs::Twist::ConstPtr &arm_msg)
        {
            ROS_INFO("in callback arm");
            s = arm_msg->angular.z;
        }

        public: void write(){
          this->joint_0->SetPosition(0,goal_0*3.14159265/180);
          this->joint_1->SetPosition(0,goal_1*3.14159265/180);
          this->joint_2->SetPosition(0,goal_2*3.14159265/180);
          this->joint_3->SetPosition(0,goal_3*3.14159265/180);
          goal_0 = arrays[0][i];
          goal_1 = arrays[1][i];
          goal_2 = arrays[2][i];
          goal_3 = arrays[3][i];
        }

        public: bool check_error(){
          err_0 = goal_0 - this->joint_0->GetAngle(0).Degree() ;
          err_1 = goal_1 - this->joint_1->GetAngle(0).Degree() ;
          err_2 = goal_2 - this->joint_2->GetAngle(0).Degree() ;
          err_3 = goal_3 - this->joint_3->GetAngle(0).Degree() ;
          // err_4 = goal_4 - this->joint_4->GetAngle(0).Degree() ;
          // ROS_INFO("%f", this->joint_1->GetAngle(0).Degree());
          // ROS_INFO(err_1);
          if (abs(err_1)<0.5)
            return 0;
          else
            return 1;
        }

        public: void set_speed(){
          vel_0 = (arrays[0][i] - arrays[0][i-1])/300;
          vel_1 = (arrays[1][i] - arrays[1][i-1])/300;
          vel_2 = (arrays[2][i] - arrays[2][i-1])/300;
          vel_3 = (arrays[3][i] - arrays[3][i-1])/300;
          this->joint_0->SetVelocity(0,vel_0);
          this->joint_1->SetVelocity(0,vel_1);
          this->joint_2->SetVelocity(0,vel_2);
          this->joint_3->SetVelocity(0,vel_3);
          // vel_4 = (arrays[4][i] - arrays[4][i-1])/50;
        }




        public: void Load(physics::ModelPtr _model, sdf::ElementPtr /*_sdf*/)       // passing pointer of the model
        {
            ROS_INFO("in load");

            this->model = _model;
            this->world = _model->GetWorld();


            unsigned int c = this->model->GetJointCount();
            ROS_INFO("%d", c);
                                                                                  // joint pointers mapped to the SDF joints
            this->joint_0 = this->model->GetJoint("shoulder_revolute_1");         // +vel +ang forward
            this->joint_1 = this->model->GetJoint("shoulder_revolute_2");         // +vel +ang outward
            this->joint_2 = this->model->GetJoint("elbow_revolute");              // +vel +ang outward
            this->joint_3 = this->model->GetJoint("wrist_revolute_1");            // +vel +ang outward
            this->joint_4 = this->model->GetJoint("wrist_revolute_2");            // joint problem


        if (!ros::isInitialized())
            {
                ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
                << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
                return;
            }

            this->rosNode.reset(new ros::NodeHandle("avitra"));
            ros::SubscribeOptions so = ros::SubscribeOptions::create<geometry_msgs::Twist>("vel_cmd", 1, boost::bind(&BasePlugin::avitra_callback, this, _1), ros::VoidPtr(), &this->rosQueue);
            this->rosSub = this->rosNode->subscribe(so);
            ros::SubscribeOptions so1 = ros::SubscribeOptions::create<geometry_msgs::Twist>("arm_con", 1, boost::bind(&BasePlugin::arm_callback, this, _1), ros::VoidPtr(), &this->rosQueue);
            this->rosSub1 = this->rosNode->subscribe(so1);
            // Listen to the update event. This event is broadcast every
            // simulation iteration.
            this->rosQueueThread = std::thread(std::bind(&BasePlugin::QueueThread, this));
            this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&BasePlugin::OnUpdate, this, _1));
        }


        public: void OnUpdate(const common::UpdateInfo & /*_info*/)             // OnUpdate continuous loop to update simulation
        {                                                                       // simulation is stopped when the control is passed to a loop which prevents the call to OnUpdate
            ROS_INFO("update");
            // if (abs(this->joint_4->GetAngle(0).Degree())<90){
            // this->joint_4->SetVelocity(0,0.3);
            // this->joint_0->SetPosition(0,0);
            // }
            // else
            // this->joint_4->SetPosition(0,1.57);
            //
            if (i<8){
              if (check_error()){
                set_speed();
              }
              else{
                write();
                i++;
                // sleep(1);
              }
            }
            // ROS_INFO("%d",i);

            }
            // else{
            //   this->joint_0->SetPosition(0,goal_0*3.14/180);
            //   this->joint_1->SetPosition(0,goal_1*3.14/180);
            //   this->joint_2->SetPosition(0,goal_2*3.14/180);
            //   this->joint_3->SetPosition(0,goal_3*3.14/180);
            // }

    };

    // Register this plugin with the simulator
    GZ_REGISTER_MODEL_PLUGIN(BasePlugin)
}
